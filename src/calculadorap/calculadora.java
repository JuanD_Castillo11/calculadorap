
	package calculadorap;

	import java.awt.GridLayout;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;

	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JTextField;

	public class calculadora extends JFrame implements ActionListener {

	    private static final long serialVersionUID = 1L;

	    private JTextField numero1g;
	    private JTextField numero2g;
	    private JTextField resultadog;
	    private JButton sumaButton;
	    private JButton restButton;
	    private JButton multButton;
	    private JButton diviButton;

	    public calculadora() {
	        setTitle("Calculadora");
	        setLayout(new GridLayout(15, 25));

	        numero1g = new JTextField(10);
	        numero2g = new JTextField(10);
	        resultadog = new JTextField(10);
	        resultadog.setEditable(false);
	        
	        sumaButton = new JButton("+");
	        sumaButton.addActionListener(this);
	        
	        restButton = new JButton("-");
	        restButton.addActionListener(this);
	        
	        multButton = new JButton("*");
	        multButton.addActionListener(this);
	        
	        diviButton = new JButton("/");
	        diviButton.addActionListener(this);

	        add(new JLabel("Número 1:"));
	        add(numero1g);
	        add(new JLabel("Número 2:"));
	        add(numero2g);
	        add(new JLabel("Resultado:"));
	        add(resultadog);
	        add(sumaButton);
	        add(restButton);
	        add(multButton);
	        add(diviButton);

	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        pack();
	        setVisible(true);
	    }

	    public void actionPerformed(ActionEvent e) {
	        double num1 = Double.parseDouble(numero1g.getText());
	        double num2 = Double.parseDouble(numero2g.getText());
	        double result = 0;

	        Operacion operacion = null;
	        if (e.getSource() == sumaButton) {
	            operacion = (Operacion) new suma();
	        } else if (e.getSource() == restButton) {
	            operacion = (Operacion) new resta();
	        } else if (e.getSource() == multButton) {
	            operacion = (Operacion) new multiplicacion();
	        } else if (e.getSource() == diviButton) {
	            operacion = (Operacion) new division();
	        }

	        if (operacion != null) {
	            try {
	                result = new calcular().procedimientos(operacion, num1, num2);
	                resultadog.setText(String.valueOf(result));
	            } catch (ArithmeticException ex) {
	                resultadog.setText("Error: " + ex.getMessage());
	            }
	        }
	    }

	    public static void main(String[] args) {
	        new calculadora();
	    }
	}

